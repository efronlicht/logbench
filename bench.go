package main

import (
	"bytes"
	"context"
	"encoding/csv"
	"fmt"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"os/signal"
	"regexp"
	"strings"
	"time"
)

func main() {
	var buf bytes.Buffer // accumulated output: formatted to CSV at end of program.
	ctx, _ := context.WithTimeout(context.Background(), 24*time.Hour)
	ctx, cancel := signal.NotifyContext(ctx, os.Interrupt)
	defer cancel()

	type Opt struct {
		size    string
		pool    bool
		cc      int
		errRate float64
		logger  string
	}
	var opts []Opt
	for _, size := range []string{"1.00KiB", "512.00KiB", "1.00MiB", "32.00MiB"} {
		for _, pool := range []bool{false, true} {
			for _, cc := range []int{8, 64, 4096} {
				for _, errRate := range []float64{0.01, 0.1, 0.5} {
					for _, logger := range []string{"zap", "logrus", "zerolog", "slog/untyped", "slog/typed", "zap", "zap/sugar", "noop"} {
						opts = append(opts, Opt{size, pool, cc, errRate, logger})
					}
				}
			}
		}
	}
	log.Printf("built test set of %v", len(opts))
	log.Printf("shuffling...")
	// Shuffle the order of the tests so as not to bias the results and to give a better estimate of remaining time.
	// (some of the tests are much slower than others)
	rand.Shuffle(len(opts), func(i, j int) { opts[i], opts[j] = opts[j], opts[i] })
	start := time.Now()
	var failures int
	for i, opt := range opts {
		if failures > 10 || ctx.Err() != nil {
			log.Printf("too many failures or context expired, exiting")
			break
		}
		log.Printf("%d/%d: %v: start", i, len(opts), opt)
		thisStart := time.Now()
		logger, size, pool, cc, errRate := opt.logger, opt.size, opt.pool, opt.cc, opt.errRate
		cmd := exec.CommandContext(ctx, "go", "test", "-v", "-timeout=1h", "-bench", ".", "-benchtime=3s",
			fmt.Sprintf("-logger=%v", logger),
			fmt.Sprintf("-size=%v", size),
			fmt.Sprintf("-pool=%v", pool),
			fmt.Sprintf("-cc=%v", cc),
			fmt.Sprintf("-errRate=%v", errRate))

		dstDir := "./results/" + logger
		must(os.MkdirAll(dstDir, 0755))

		cmd.Stdout = &buf
		cmd.Stderr = os.Stderr
		if err := cmd.Run(); err != nil {
			failures++
			log.Printf("%d/%d: %v: failed: %v", i, len(opts), opt, err)
			time.Sleep(time.Second)
			continue
		}
		thisDone := time.Since(thisStart).Round(time.Second)
		totalElapsed := time.Since(start).Round(time.Second)
		log.Printf("%d/%d: %v: done in %v (total elapsed %v)", i, len(opts), opt, thisDone, totalElapsed)

		timePer := float64(totalElapsed) / float64(i+1)
		finish := time.Duration(timePer * float64(len(opts)-(i+1)))
		if finish > time.Hour {
			finish = finish.Round(10 * time.Minute)
			log.Printf("expected finish in ~%v (low confidence)", finish)
		} else if finish > 10*time.Minute {
			finish = finish.Round(5 * time.Minute)
			log.Printf("expected finish in ~%v (medium confidence)", finish)
		} else {
			finish = finish.Round(time.Minute)
			log.Printf("expected finish in ~%v (high confidence)", finish)
		}
	}

	{ // write CSV
		f, err := os.Create("./results/all.csv")
		must(err)
		cw := csv.NewWriter(f)
		defer cw.Flush()

		var wroteLabels bool

		var csvfields []string
		loggerName := regexp.MustCompile(`(zap/sugar|zap|logrus|zerolog|slog/untyped|slog/typed|noop)`)

		for _, line := range strings.Split(buf.String(), "\n") {
			if !strings.Contains(line, "ns/op") {
				continue
			}
			f := strings.Fields(line)
			if !wroteLabels {
				wroteLabels = true
				csvfields = append(csvfields[:0], "logger", "n")
				for i := 3; i < len(f); i += 2 {
					csvfields = append(csvfields, f[i])
				}
				must(cw.Write(csvfields))
			}

			csvfields = append(csvfields[:0], loggerName.FindString(f[0]), f[1])
			for i := 2; i < len(f); i += 2 {
				csvfields = append(csvfields, f[i])
			}
			must(cw.Write(csvfields))
		}
		log.Printf("wrote %v", f.Name())

	}

}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
