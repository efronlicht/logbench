package main_test

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"log/slog"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"runtime/pprof"
	"strconv"
	"strings"
	"sync"
	"testing"
	"time"
	"unsafe"

	"github.com/rs/zerolog"
	"github.com/sirupsen/logrus"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// cli flags
type (
	sizeflag int

	loggerFlag struct {
		name string
		log  func(status int, duration time.Duration, body []byte)
	}
)

// UnitVal returns the size in a human-readable unit. E.G, 1024*1024*1024 -> 1.0, "GiB"
func (sizeflag) UnitVal() (float64, string) {
	switch {
	case size < 1024:
		return float64(size), "B"
	case size < 1024*1024:
		return float64(size) / 1024, "KiB"
	case size < 1024*1024*1024:
		return float64(size) / (1024 * 1024), "MiB"
	default:
		return float64(size) / (1024 * 1024 * 1024), "GiB"
	}
}

func (s *sizeflag) String() string {
	switch {
	case *s < 1024*1024:
		return fmt.Sprintf("%.2fKiB", float64(*s)/1024)
	case *s < 1024*1024*1024:
		return fmt.Sprintf("%.2fMiB", float64(*s)/(1024*1024))
	default:
		return fmt.Sprintf("%.2fGiB", float64(*s)/(1024*1024*1024))
	}
}

func (sf *sizeflag) Set(s string) error {
	var mul float64 = 1
	raw := s
	for _, v := range []struct {
		suffix string
		mul    float64
	}{
		{"KB", 1000},
		{"MB", 1000 * 1000},
		{"GB", 1000 * 1000 * 1000},
		{"K", 1024},
		{"M", 1024 * 1024},
		{"G", 1024 * 1024 * 1024},
		{"KiB", 1024},
		{"MiB", 1024 * 1024},
		{"GiB", 1024 * 1024 * 1024},
	} {
		if strings.HasSuffix(s, v.suffix) {
			mul = v.mul
			raw = strings.TrimSuffix(s, v.suffix)
			break
		}
	}
	f, err := strconv.ParseFloat(raw, 64)
	if err != nil {
		return err
	}
	*sf = sizeflag(f * mul)
	return nil
}

func (l *loggerFlag) Set(s string) error {
	s = strings.ToLower(s)
	switch s {
	case "log":
		logger := log.New(discard, "", 0)
		l.log = func(status int, duration time.Duration, body []byte) {
			logger.Printf("some log message: key1=%v, key2=%v, key3=%v", status, duration, body)
		}
	case "slog/untyped":
		logger := slog.New(slog.NewJSONHandler(discard, &slog.HandlerOptions{}))
		l.log = func(status int, duration time.Duration, body []byte) {
			logger.Info("some log message", "status", status, "duration", duration, "body", body)
		}
	case "slog/typed":
		logger := slog.New(slog.NewJSONHandler(discard, &slog.HandlerOptions{}))
		l.log = func(status int, duration time.Duration, body []byte) {
			logger.Info("some log message", slog.Int("status", status), slog.Duration("duration", duration), slog.String("body", unsafe.String(unsafe.SliceData(body), len(body)))) // we don't want this allocation in the benchmark.
		}
	case "zerolog":
		logger := zerolog.New(discard)
		l.log = func(status int, duration time.Duration, body []byte) {
			logger.Info().Int("status", status).Dur("elapsed", duration).Bytes("body", body).Msg("some log message")
		}
	case "logrus":
		logger := logrus.New()
		logger.Out = discard
		l.log = func(status int, duration time.Duration, body []byte) {
			logger.WithFields(logrus.Fields{
				"status":  status,
				"elapsed": duration,
				"body":    body,
			}).Info("some log message")
		}
	case "zap":
		zapcfg := zap.NewProductionEncoderConfig()
		zcore := zapcore.NewCore(zapcore.NewJSONEncoder(zapcfg), zapcore.AddSync(discard), zap.DebugLevel)
		logger := zap.New(zcore)
		l.log = func(status int, duration time.Duration, body []byte) {
			logger.Info("some log message", zap.Int("status", status), zap.Duration("duration", duration), zap.ByteString("body", body))
		}
	case "noop":
		l.log = func(status int, duration time.Duration, body []byte) {} // noop
	case "zap/sugar":
		zapcfg := zap.NewProductionEncoderConfig()
		zcore := zapcore.NewCore(zapcore.NewJSONEncoder(zapcfg), zapcore.AddSync(discard), zap.DebugLevel)
		logger := zap.New(zcore).Sugar()
		l.log = func(status int, duration time.Duration, body []byte) {
			logger.Infof("some log message: key1=%v, key2=%v, key3=%v", status, duration, body)
		}
	default:
		return fmt.Errorf("unknown logger %v: expected one of noop, log, slog/untyped, slog/typed, zerolog, logrus, zap, zap/sugar", s)
	}
	l.name = s
	return nil
}

func (l *loggerFlag) String() string { return l.name }

// CLI flags
var (
	logFlag     = loggerFlag{name: "noop", log: func(status int, duration time.Duration, body []byte) {}}
	size        = sizeflag(16 * 1024)
	concurrency int
	errRate     float64
	poolBuffers bool
)

func BenchmarkResponseLogging(b *testing.B) {
	simultaneousProcs := concurrency * runtime.GOMAXPROCS(0)
	logger := logFlag.log
	logName := logFlag.name
	var onoff = "p-off"
	if poolBuffers {
		onoff = "p-on"
	}
	name := fmt.Sprintf("%s/%s/%s/E%2v%%/CC-%04d", logName, onoff, &size, errRate*100, simultaneousProcs)

	b.Run(name, func(b *testing.B) {
		b.ResetTimer()

		var handler http.HandlerFunc = func(w http.ResponseWriter, r *http.Request) {
			var reqBody struct {
				Status int    `json:"status"`
				Body   string `json:"body"`
			}
			b, _ := io.ReadAll(r.Body)
			_ = json.Unmarshal(b, &reqBody)
			w.WriteHeader(reqBody.Status)
			_, _ = w.Write([]byte(reqBody.Body))

			if reqBody.Status >= 400 {
				logger(reqBody.Status, time.Since(time.Now()), b)
			}
		}

		server := http.Server{
			Addr:         ":6483",
			Handler:      middleware(handler, logger, poolBuffers),
			ReadTimeout:  2 * time.Minute,
			WriteTimeout: 2 * time.Minute,
		}
		b.Cleanup(func() { server.Close() })
		b.Cleanup(client.CloseIdleConnections)

		go func() {
			if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
				panic(err)
			}
		}()

		var okBody, errBody []byte
		{
			type reqBody = struct {
				Status int    `json:"status"`
				Body   string `json:"body"`
			}
			var err error
			okBody, err = json.Marshal(reqBody{200, strings.Repeat("o", int(size))})
			if err != nil {
				panic(err)
			}

			errBody, err = json.Marshal(reqBody{500, strings.Repeat("e", int(size))})
			if err != nil {
				panic(err)
			}
		}

		b.ResetTimer()
		b.ReportAllocs()
		b.SetParallelism(concurrency)
		b.RunParallel(func(pb *testing.PB) {
			rng := rand.New(rand.NewSource(time.Now().UnixNano())) // new seed for each goroutine so as not to have mutex contention.

			for pb.Next() {
				if rng.Float64() < errRate {
					req, _ := http.NewRequest(http.MethodPost, "http://localhost:6483", bytes.NewReader(errBody))
					_, _ = client.Do(req)
				} else {
					req, _ := http.NewRequest(http.MethodPost, "http://localhost:6483", bytes.NewReader(okBody))
					_, _ = client.Do(req)
				}
			}
		})
		b.StopTimer()
		stats := new(runtime.MemStats)
		runtime.ReadMemStats(stats)
		if poolBuffers {
			b.ReportMetric(1, "pool")
		} else {
			b.ReportMetric(0, "pool")
		}
		b.ReportMetric(errRate, "error-rate")
		b.ReportMetric(float64(simultaneousProcs), "simultaneous-procs")
		b.ReportMetric(float64(size), "size")
		b.ReportMetric(float64(stats.HeapAlloc), "heap-alloc")
		b.ReportMetric(float64(stats.HeapIdle), "heap-idle")
		b.ReportMetric(float64(stats.HeapSys), "heap-sys")
		b.ReportMetric(float64(stats.NumGC), "num-gc")
		b.ReportMetric(float64(stats.TotalAlloc), "total-alloc")

	})
}

// Register additional flags.
func TestMain(m *testing.M) {
	runtime.MemProfileRate = 1 // track every allocation.
	testing.Init()
	flag.Var(&size, "size", "size of the request body (e.g, 1KiB, 1MiB, 1GiB)")
	flag.IntVar(&concurrency, "cc", 1, "concurrent requests per processor")
	flag.Float64Var(&errRate, "errRate", 0.1, "error rate (0-1)")
	flag.BoolVar(&poolBuffers, "pool", false, "use a sync.Pool for buffers")
	flag.Var(&logFlag, "logger", "logger to use (noop, log, slog/untyped, slog/typed, zerolog, logrus, zap, zap/sugar)")

	flag.Parse()
	switch {
	case errRate < 0 || errRate > 1:
		log.Printf("invalid error rate %v: expected 0-1", errRate)
	case concurrency < 1:
		log.Printf("invalid concurrency %v: expected >0", concurrency)
	case size < 1:
		log.Printf("invalid size %v: expected >0", size)
	case logFlag.log == nil || logFlag.name == "":
		log.Printf("no logger specified")
	default:
		code := m.Run()
		runtime.GC()
		writeMemProfile(fmt.Sprintf("results/%s/%v_%v_%v_%v", logFlag.name, poolBuffers, &size, errRate*100, concurrency))
		os.Exit(code)
	}
	flag.Usage()
	os.Exit(1)
}

type DiscardingResponseWriter struct {
	h http.Header
}

func (d DiscardingResponseWriter) Header() http.Header {
	return d.h
}

//go:noinline
func (d DiscardingResponseWriter) Write(b []byte) (int, error) {
	return len(b), nil
}

//go:noinline
func (d DiscardingResponseWriter) WriteHeader(statusCode int) {}

var client = http.Client{
	Timeout: 10 * time.Second,
}

// Discard is an io.Writer that discards all data written to it.
// We don't use io.Discard just in case the logging implementations have some special case for it.
type Discard struct{}

var discard io.Writer = Discard{}

func (d Discard) Write(b []byte) (int, error)       { return len(b), nil }
func (d Discard) Flush() error                      { return nil }
func (d Discard) Sync() error                       { return nil }
func (d Discard) WriteString(s string) (int, error) { return len(s), nil }
func (d Discard) WriteByte(b byte) error            { return nil }

type loggingResponseWriter struct {
	rw         http.ResponseWriter
	buf        *bytes.Buffer
	statusCode int
}

func (lrw *loggingResponseWriter) Header() http.Header {
	return lrw.rw.Header()
}
func (lrw *loggingResponseWriter) WriteHeader(statusCode int) {
	if lrw.statusCode == 0 { // we shouldn't be able to write the header twice, put pass it through anyway.
		lrw.statusCode = statusCode
	}
	lrw.rw.WriteHeader(statusCode)
}
func (lrw *loggingResponseWriter) Write(b []byte) (int, error) {
	if lrw.statusCode == 0 {
		lrw.WriteHeader(http.StatusOK)
	}
	if lrw.statusCode >= 400 {
		_, _ = lrw.buf.Write(b)
	}
	return lrw.rw.Write(b)
}

var pool = sync.Pool{New: func() interface{} { return new(bytes.Buffer) }}

func middleware(h http.HandlerFunc, logger func(status int, duration time.Duration, body []byte), usePool bool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		rw := &loggingResponseWriter{rw: w}
		if usePool {
			rw.buf = pool.Get().(*bytes.Buffer)
			defer func() {
				rw.buf.Reset()
				pool.Put(rw.buf)
			}()
		} else {
			rw.buf = new(bytes.Buffer)
		}
		h(rw, r)
		if rw.statusCode >= 400 {
			logger(rw.statusCode, time.Since(start), rw.buf.Bytes())
		}
	}
}

func writeMemProfile(name string) {
	name = "./" + name + ".memprof"
	if err := os.MkdirAll(filepath.Dir(name), 0755); err != nil {
		panic(err)
	}
	f, err := os.Create(name)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	if err := pprof.WriteHeapProfile(f); err != nil {
		panic(err)
	}
	log.Printf("wrote memory profile to %v", name)

}
