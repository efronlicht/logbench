# logbench

Logbench simulates logging HTTP responses on a web server. It consists of two parts: `log_test.go`, which contains the actual benchmarks, and `bench.go`, which repeatedly runs the benchmarks with different parameters.


## log_test.go

log-test benchmarks the performance of various logging libraries as they record HTTP responses on error. It works as follows:

- A standard `net/http` web server is started on port 6483, which reads JSON bodies in the following format:
```json
{
    "status": 200,
    "body": "some text"
}
```
The server responds by sending the `body` field back as the response body, and the `status` field as the HTTP status code. It also logs the request if it was sent a 4xx or 5xx status code.

The benchmarks in log_test.go send requests to this server using the following parameters (CLI flags) in addition to standard Go test/benchmark flags:


- cc int
    concurrent requests per processor (default 1)
- errRate float
  
    error rate (0-1) (default 0.1)
- logger string
  
    logger to use (noop, log, slog/untyped, slog/typed, zerolog, logrus, zap, zap/sugar)
- pool
  
    use a sync.Pool for buffers
- size value
    size of the request body (e.g, 1KiB, 1MiB, 1GiB) (default 16.00KiB)


## bench.go

bench.go runs the benchmarks in log_test.go with various parameters, outputting results to the ./results directory. The benchmark output is in files named `results/<logger>/<pool>_<size>_<errrate>_<concurrency>.txt`, and a `pprof` heap profile is generated at `results/<logger>/<pool>_<size>_<errrate>_<concurrency>.memprof`, which can be examined with `go tool pprof`. For example, `results/logrus/true_16.00KiB_0.1_1.txt` contains the benchmark of running the logrus logger with a 16KiB request body, a 10% error rate, and 1 concurrent request per processor, and `results/logrus/true_16.00KiB_0.1_1.memprof` contains the heap profile for that run.

Additionally, it collects ALL the benchmark output into a single CSV file, `results/all.csv`, ready for use by other tools. An example of the output is stored (zipped) in this repository as `log_benchmarks.csv.gz`.

## notes on design:
Go's memory profiling tools are not good at profiling function calls independently, so instead of making many different benchmark functions, we tune a single benchmark function with different parameters and run a separate executable for each. This is why we have a separate `bench.go` file instead of just using `go test -bench .` 